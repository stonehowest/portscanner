from flask import redirect
from flask import Flask
from flask.ext.login import LoginManager
from flask.ext.bootstrap import Bootstrap
import database.initdatabase as initdatabase

import database.model as model
from controller.filter_controller import filter_controller
from controller.notification_controller import notification_controller
from controller.protocol_controller import protocol_controller
from controller.server_controller import server_controller
from controller.index_controller import index_controller
from controller.login_controller import login_controller
from controller.configuration_controller import configuration_controller
from controller.static_controler import static_controller
from controller.feedback_controller import feedback_controller
from controller.accounts_controller import accounts_controller
from controller.scan_controller import scan_controller
from controller.profile_controller import profile_controller

app = Flask(__name__)
app.register_blueprint(feedback_controller, url_prefix="/feedback")
app.register_blueprint(filter_controller, url_prefix="/filters")
app.register_blueprint(notification_controller, url_prefix="/notifications")
app.register_blueprint(protocol_controller, url_prefix="/protocols")
app.register_blueprint(server_controller, url_prefix ="/servers")
app.register_blueprint(index_controller, url_prefix ="/")
app.register_blueprint(login_controller, url_prefix="/login")
app.register_blueprint(configuration_controller, url_prefix="/configuration")
app.register_blueprint(static_controller, url_prefix="/static")
app.register_blueprint(accounts_controller, url_prefix="/accounts")
app.register_blueprint(scan_controller, url_prefix="/scan")
app.register_blueprint(profile_controller, url_prefix="/profile")

@app.before_request
def _db_connect():
    model.connect()


# This hook ensures that the connection is closed when we've finished
# processing the request.

login_manager = LoginManager();
@app.teardown_request
def _db_close(exc):
    if not model.is_closed():
        model.close()

@login_manager.user_loader
def load_user(user_id):
    try:
        return model.User.select().where(model.User.id == user_id).get();
    except Exception:
        return None
@app.errorhandler(401)
def page_no_access(e):
    return redirect("/login/")

if __name__ == '__main__':
    initdatabase.check()
    app.debug = True
    Bootstrap(app)
    app.config['SESSION_TYPE'] = 'memcached'
    app.config['SECRET_KEY'] = '{=fCe~g_[95S6Wb7:5,N_V!/63zL9_-4'
    login_manager.init_app(app)
    app.run()

