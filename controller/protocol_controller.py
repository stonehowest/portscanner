from flask import Blueprint, render_template, request
from flask.ext.paginate import Pagination
import database.webdata as data
from database.model import *
import json
from flask.ext.login import login_required, current_user

protocol_controller = Blueprint('protocol_controller', __name__)

##Check if are is a search and makes a normal pages or a pages search results page
@protocol_controller.route('/')
@login_required
def protocol():
    search = False
    q = request.args.get('q')
    if q:
        search = True
    try:
        page = int(request.args.get('page', 1))
    except ValueError:
        page = 1
    port = request.args.get("port")
    service = request.args.get("service")
    protocol = request.args.get("protocol")
    if port or service or protocol:
        if port:
            protocols = Protocol.select().order_by(Protocol.port).where(Protocol.port == int(port)).paginate(page, 30)
            total = Protocol.select().order_by(Protocol.port).where(Protocol.port == int(port)).count()
        if service:
            protocols = Protocol.select().order_by(Protocol.port).where(Protocol.service % service).paginate(page, 30)
            total = Protocol.select().order_by(Protocol.port).where(Protocol.service % service).count()
        if protocol:
            protocols = Protocol.select().order_by(Protocol.port).where(Protocol.protocol == protocol).paginate(page, 30)
            total = Protocol.select().order_by(Protocol.port).where(Protocol.protocol == protocol).count()
    else:
        protocols = Protocol.select().order_by(Protocol.port).paginate(page, 30)
        total = Protocol.select().order_by(Protocol.port).count()
    pagination = Pagination(page=page,
                            css_framework="bootstrap3",
                            perPage=30,
                            total=int(total/3),
                            search=search,
                            record_name='protocols')
    search_json = []
    for row in Protocol.select(Protocol.port, Protocol.service):
        if row.port not in search_json:
            search_json.append(row.port)
    search_json.append('tcp')
    search_json.append('udp')
    return render_template("views/protocols.html",
                           protocols=protocols,
                           pagination=pagination,
                           search=json.dumps(search_json))

##Calls the function in webdata.py
@protocol_controller.route('/add', methods=['POST'])
@login_required
def add():
    port = request.form['port']
    if not request.form['service'] or not request.form['protocol'] or not port or not request.form['comment']:
        return json.dumps({"status": "field"})
    port = int(port)
    if port<0:
       return json.dumps({"status": "fail-underzero"})
    return data.insert_protocol(request.form['service'], request.form['protocol'], request.form['port'], request.form['comment'], current_user.get_login())

##Calls the function in webdata.py
@protocol_controller.route('/delete', methods=['POST'])
@login_required
def delete():
    return data.remove_protocol(request.form['id'])

##The following functions edit the values of protocols
@protocol_controller.route('/edit_comment', methods=['POST'])
@login_required
def edit_comment():
    id = request.form['pk']
    comment = request.form['value']
    Protocol.update(comment=comment,user_comment=current_user.get_login()).where(Protocol.id == id).execute()
    return json.dumps({"user": current_user.get_login()})

@protocol_controller.route('/edit_port', methods=['POST'])
@login_required
def edit_port():
    id =  request.form['pk']
    port = request.form['value']
    Protocol.update(port=port).where(Protocol.id == id).execute()
    return json.dumps({})

@protocol_controller.route('/edit_service', methods=['POST'])
@login_required
def edit_service():
    id = request.form['pk']
    service = request.form['value']
    Protocol.update(service=service).where(Protocol.id == id).execute()
    return json.dumps({})

@protocol_controller.route('/edit_protocol', methods=['POST'])
@login_required
def edit_protocol():
    id = request.form['pk']
    protocol = request.form['value']
    Protocol.update(protocol=protocol).where(Protocol.id == id).execute()
    return json.dumps({})