from flask import Blueprint, render_template, request
from database.model import *
import database.webdata as webdata
from flask.ext.login import login_required, current_user
import json
import hashlib


profile_controller = Blueprint('profile_controller', __name__)

@profile_controller.route('/')
@login_required
def profile():
    return render_template("views/profile.html",
                               user=current_user,
                               search=webdata.get_json_search())

#change username
@profile_controller.route("/user_login", methods=['POST'])
@login_required
def user_login():
    login = request.form["value"]
    id = request.form['pk']
    return webdata.user_login(id, login)

#change first_name
@profile_controller.route("/user_first_name", methods=['POST'])
@login_required
def user_first_name():
    first_name = request.form["value"]
    id = request.form['pk']
    return webdata.user_first_name(id, first_name)

#change last_name
@profile_controller.route("/user_last_name", methods=['POST'])
@login_required
def user_last_name():
    last_name = request.form["value"]
    id = request.form['pk']
    return webdata.user_last_name(id, last_name)

#change email
@profile_controller.route("/user_email", methods=['POST'])
@login_required
def user_email():
    email = request.form["value"]
    id = request.form['pk']
    return webdata.user_email(id, email)

#change password
@profile_controller.route('/change_password', methods=['POST'])
@login_required
def change_password():
    newpassword = request.form["new"]
    oldpassword = request.form["old"]
    #check if field are not empty
    if not newpassword or not oldpassword:
        return json.dumps({"status": "field"})
    else:
        #check if password is strong enough
        if webdata.CheckPassword(newpassword):
            #check if old password if correct and than change password
            changed = User.update(password = hashlib.sha1(newpassword.encode()).hexdigest()).where(User.id == current_user.get_id(), User.password == hashlib.sha1(oldpassword.encode()).hexdigest() ).execute()
            if changed:
                return json.dumps({"status":"changed"})
            else:
                return json.dumps({"status":"password-fail"})
        else:
            return json.dumps({"status": "check-password-failed"})
