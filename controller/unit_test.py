__author__ = 'matthias'

from unittest import TestCase
from playhouse.test_utils import test_database
from peewee import *
from database.Model import Protocol

test_db = SqliteDatabase(":nmap_db:")

class MyTestCase(TestCase):
    def setUp(self):
        super(MyTestCase, self).setUp()

    def test_create_protocol(self):
        for i in range(10):
            Protocol.create(service='service-%d' % i, port='port-%d' % i, comment='comment-%d' % i)
        print("test_create_protocol successfull")

    def run(self, result=None):
        with test_database(test_db, Protocol):
                super(MyTestCase, self).run(result )

    def test_timeline(self):
        self.test_create_protocol()
        self.assertEqual(Protocol.timeline("service-0") [...])
