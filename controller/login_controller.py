import hashlib
from flask import Blueprint, render_template, request, redirect, flash
from flask.ext.login import login_user, logout_user,login_required, current_user
from database.model import *

login_controller = Blueprint('login_controller', __name__)

@login_controller.route("/", methods=["GET"])
def login_get():
    return render_template("views/login.html")

@login_controller.route("/", methods=["POST"])
def login():
    try:
        user = User.select().where(User.active == 1,
                                   User.login == request.form['username'],
                                   User.password == hashlib.sha1(request.form['password'].encode('utf-8')).hexdigest()).get()
        if user:
            login_user(user)
            if user.is_authenticated():
                return redirect("/")
    except:
        return render_template("views/login.html", warning="Login failed, user or password not valid or user is not active")

@login_controller.route("/logout")
@login_required
def logout():
    logout_user()
    flash("Logged out")
    return render_template("views/login.html", message="Logged out")


