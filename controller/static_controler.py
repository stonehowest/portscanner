from flask import send_from_directory, Blueprint

static_controller = Blueprint('static_controller', __name__)

@static_controller.route("/static/<path:path>")
def send_static(path):
    return send_from_directory('static', path)