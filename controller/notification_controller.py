from flask import Blueprint, render_template, request
import database.webdata as data
import json, datetime, re, netaddr
import database.model as model
from flask.ext.login import login_required, current_user
from flask.ext.paginate import Pagination
from peewee import fn

notification_controller = Blueprint('notification_controller', __name__)

@notification_controller.route('/')
@login_required
def notification():
    search = False
    q = request.args.get('q')
    if q:
        search = True
    try:
        page = int(request.args.get('page', 1))
    except ValueError:
        page = 1
    network = request.args.get("network")
    if network:
        if not re.match("[a-zA-Z]+", network):
            ips = list(netaddr.IPNetwork(network))
            alerts = model.Alert_ip.select(model.Alert_ip,
                                           model.Alert_port,
                                           model.Hostname_Ip,
                                           fn.COUNT(model.Alert_port.id).alias("number"))\
                .where(model.Alert_ip.ip << ips)\
                .paginate(page,30)\
                .join(model.Alert_port, "left outer", on=model.Alert_port.alert_ip == model.Alert_ip.id)\
                .join(model.Hostname_Ip, on=model.Alert_ip.ip == model.Hostname_Ip.ip)\
                .group_by(model.Alert_ip.ip)\
                .aggregate_rows()
            total = model.Alert_ip.select().where(model.Alert_ip.ip << ips).count()
        else:
            alerts = model.Alert_ip.select(model.Alert_ip,
                                           model.Alert_port,
                                           model.Hostname_Ip,
                                           fn.COUNT(model.Alert_port.id).alias("number"))\
                .where(model.Hostname_Ip.hostname % network)\
                .paginate(page,30)\
                .join(model.Alert_port, "left outer", on=model.Alert_port.alert_ip == model.Alert_ip.id)\
                .join(model.Hostname_Ip, on=model.Alert_ip.ip == model.Hostname_Ip.ip)\
                .group_by(model.Alert_ip.ip).aggregate_rows()
            total = 1
    else:
        alerts = model.Alert_ip.select(model.Alert_ip,
                                       model.Alert_port,
                                       model.Hostname_Ip,
                                       fn.COUNT(model.Alert_port.id).alias("number"))\
            .paginate(page,30)\
            .join(model.Alert_port, "left outer", on=model.Alert_port.alert_ip == model.Alert_ip.id)\
            .join(model.Hostname_Ip, on=model.Alert_ip.ip == model.Hostname_Ip.ip)\
            .group_by(model.Alert_ip.ip).aggregate_rows()
        total = model.Alert_ip.select().count()
    pagination = Pagination(page=page,
                            css_framework="bootstrap3",
                            perPage=30,
                            total=int(total/3),
                            search=search,
                            record_name='notificaties')
    return render_template("views/notifications.html",
                           now=datetime.datetime.now(),
                           title="notifications",
                           notificaties=alerts,
                           pagination=pagination,
                           network_filter=model.History_networks.select(),
                           search=data.get_json_search(),
                           network=network)

@notification_controller.route('/delete_alert_ip', methods=['POST'])
@login_required
def delete_alert_ip():
    id = request.form['id']
    return data.delete_alert_ip(id)

@notification_controller.route('/delete_alert_port', methods=['POST'])
@login_required
def delete_alert_port():
    id = request.form['id']
    return data.delete_alert_port(id)

@notification_controller.route('/filter_alert_ip', methods=['POST'])
@login_required
def filter_alert_ip():
    ip = request.form['ip']
    return data.insert_in_filter_alert_ip(ip)

#notifcation  update
@notification_controller.route('/ip_edit_comment' , methods=['POST'])
@login_required
def ip_edit_comment():
    id = request.form['pk']
    comment = request.form["value"]
    model.Alert_ip.update(comment=comment,
                          user_comment=current_user.get_login())\
        .where(model.Alert_ip.id == id).execute()
    return json.dumps({"user": current_user.get_login()})

@notification_controller.route('/port_edit_comment' , methods=['POST'])
@login_required
def notification_port_edit_comment():
    id = request.form['pk']
    comment = request.form["value"]
    model.Alert_port.update(comment=comment,
                            user_comment=current_user.get_login())\
        .where(model.Alert_port.id == id).execute()
    return json.dumps({"user": current_user.get_login()})

@notification_controller.route('/check_ip', methods=['POST'])
@login_required
def check_ip():
    id = request.form["id"]
    model.Alert_ip.update(is_checked=True,
                          checked_by=current_user.get_login())\
        .where(model.Alert_ip.id == id).execute()
    model.Alert_port.update(is_checked=True,
                            checked_by=current_user.get_login())\
        .where(model.Alert_port.alert_ip == id).execute()

    return json.dumps({'username': current_user.get_login()})

@notification_controller.route('/check_port', methods=['POST'])
@login_required
def check_port():
    id = request.form["id"]
    model.Alert_port.update(is_checked=True,checked_by=current_user.get_login()).where(model.Alert_port.id == id).execute()
    ip = model.Alert_port.select(model.Alert_port.alert_ip).where(model.Alert_port.id == id)
    if model.Alert_port.select().where(model.Alert_port.alert_ip == ip, ~model.Alert_port.is_checked).count() == 0:
        model.Alert_ip.update(is_checked=True, checked_by="system").where(model.Alert_ip.id == ip).execute()
    return json.dumps({'username': current_user.get_login()})

@notification_controller.route('/add_filter_ip', methods=['POST'])
@login_required
def add_filter_ip():
    ip = request.form["ip"]
    model.Rule_filter.insert(ip=ip).execute()
    if request.form["remove"]:
        #cascading---
        model.Alert_ip.delete().where(model.Alert_ip.ip == ip).execute()
    return json.dumps({})

@notification_controller.route('/add_filter_port', methods=['POST'])
@login_required
def add_filter_port():
    ip = request.form["ip"]
    port = int(request.form["port"])
    ip_id = request.form["ipId"]
    if request.form["remove"]:
        model.Alert_port.delete().where(model.Alert_port.alert_ip == ip_id,
                                        model.Alert_port.port == port).execute()
    model.Rule_filter.insert(ip=ip, port=port).execute()
    return json.dumps({})
