from flask import Blueprint, render_template, request
import json
from flask.ext.login import login_required, current_user
from netaddr import IPNetwork
from database.scannerdata import new_scan_in_db
from database.model import *

import requests

scan_controller = Blueprint('scan_controller', __name__)

def __send_request__(scanner, action = "",  data={}):
    try:
        data["key"] = scanner.key
        data = json.dumps(data)
        print(data)
        request = requests.post("http://" + scanner.uri + "/" + action, data, headers={"Content-Type": "application/json"});
        return request.json()
    except Exception as e:
        print("Could not find scanner!", e)
        return False

@scan_controller.route('/scan', methods=['POST'])
@login_required
def scan():
    network = request.form["networks"]
    scanners = list(Scanners.select().execute())
    networks = list(Networks.select().where(Networks.network == network).execute())
    addresses = []
    for network in networks:
        for address in list(IPNetwork(network.network)):
            addresses.append(address.__str__())
    addresses = iter(chunkIt(addresses, len(scanners)))
    for scanner in scanners:
        data = __send_request__(scanner,"scan",{"addresses":next(addresses)})
        print(data)
        new_scan_in_db(data["data"])
    return json.dumps({})

@scan_controller.route('/full_scan', methods=['POST'])
@login_required
def full_scan():
    scanners = list(Scanners.select().execute())
    networks = list(Networks.select().execute())
    addresses = []
    for network in networks:
        for address in list(IPNetwork(network.network)):
            addresses.append(address.__str__())
    addresses = iter(chunkIt(addresses, len(scanners)))
    for scanner in scanners:
        data = __send_request__(scanner,"scan",{"addresses":next(addresses)})
        print(data)
        new_scan_in_db(data["data"])
    return json.dumps({})

@scan_controller.route('/get_scan_progress', methods=['POST'])
@login_required
def get_scan_progress():
    scanners = Scanners.select().execute()
    totalprogress = 0;
    scannercount = 0
    for scanner in scanners:
        responseData = __send_request__(scanner)
        print(responseData)
        if (responseData["scanning"]):
            totalprogress = responseData["progress"] + totalprogress
            scannercount = 1 + scannercount
    progress = 0
    if scannercount != 0:
        progress = totalprogress / scannercount
    return json.dumps({"progress":progress, "scanning":scannercount != 0})

@scan_controller.route('/cb_port_scan', methods=['POST'])
def handle_port_scan():
    
    return None

def chunkIt(seq, num):
  avg = len(seq) / float(num)
  out = []
  last = 0.0
  while last < len(seq):
    out.append(seq[int(last):int(last + avg)])
    last += avg
  return out