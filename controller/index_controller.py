from flask import Blueprint, render_template, request
import database.webdata as data
import json
import netaddr
from peewee import fn
from flask.ext.paginate import Pagination
from flask.ext.login import login_required, current_user
import database.model as model
import datetime
import re

index_controller = Blueprint('index_controller', __name__)

@index_controller.route('/')
@login_required
def dashboard():
    search = False
    q = request.args.get('q')
    if q:
        search = True
    try:
        page = int(request.args.get('page', 1))
    except ValueError:
        page = 1
    network = request.args.get("network")
    if network:
        if not re.match("[a-zA-Z]+", network):
            ips = list(netaddr.IPNetwork(network))
            alerts = model.Alert_ip.select(model.Alert_ip,
                                           model.Alert_port,
                                           model.Hostname_Ip,
                                           fn.COUNT(model.Alert_port.id).alias("number"))\
                .where(model.Alert_ip.ip << ips)\
                .paginate(page, 30)\
                .join(model.Alert_port, "left outer", on=model.Alert_port.alert_ip == model.Alert_ip.id)\
                .join(model.Hostname_Ip, on=model.Alert_ip.ip == model.Hostname_Ip.ip)\
                .group_by(model.Alert_ip.ip)\
                .having(~model.Alert_ip.is_checked).aggregate_rows()
            total = model.Alert_ip.select().where(~model.Alert_ip.is_checked, model.Alert_ip.ip << ips).count()
        else:
            alerts = model.Alert_ip.select(model.Alert_ip,
                                           model.Alert_port,
                                           model.Hostname_Ip,
                                           fn.COUNT(model.Alert_port.id).alias("number"))\
                .where(model.Hostname_Ip.hostname % network).paginate(page, 30)\
                .join(model.Alert_port, "left outer", on=model.Alert_port.alert_ip == model.Alert_ip.id)\
                .join(model.Hostname_Ip, on=model.Alert_ip.ip == model.Hostname_Ip.ip)\
                .group_by(model.Alert_ip.ip)\
                .having(~model.Alert_ip.is_checked).aggregate_rows()
            total = 1
    else:
        ports_checked = model.Alert_port.select(model.Alert_port.id).where(~model.Alert_port.is_checked)
        alerts = model.Alert_ip.select(model.Alert_ip,
                                       model.Alert_port,
                                       fn.COUNT(model.Alert_port.id).alias("number"))\
            .paginate(page, 30)\
            .join(model.Alert_port, on=model.Alert_port.alert_ip == model.Alert_ip.id)\
            .switch(model.Alert_port)\
            .where(model.Alert_port.id << ports_checked)\
            .join(model.Hostname_Ip, on=model.Alert_ip.ip == model.Hostname_Ip.ip)\
            .group_by(model.Alert_ip.ip)
        total = model.Alert_ip.select().where(~model.Alert_ip.is_checked).count()
    pagination = Pagination(page=page,
                            css_framework="bootstrap3",
                            perPage=30,
                            total=int(total/3),
                            search=search,
                            record_name='notificaties')
    return render_template("views/dashboard.html",
                           now=datetime.datetime.now(),
                           title="Dashboard",
                           notificaties=alerts,
                           pagination=pagination,
                           network_filter=model.History_networks.select(),
                           search=data.get_json_search(),
                           network=network)

@index_controller.route('/configuration')
@login_required
def configuration():
    return render_template("views/dashboard.html",
                           title="notifications",
                           notificaties=data.get_all_notifications())

@index_controller.route('/edit_comment', methods=['POST'])
@login_required
def server_edit_comment():
    ip = request.form["pk"]
    comment = request.form["value"]
    model.Hostname_Ip.update(comment=comment,
                             user_comment=current_user.get_login())\
        .where(model.Hostname_Ip.ip == ip).execute()
    return json.dumps({"user": current_user.get_login()})

#comment nog toetevoegen!
@index_controller.route('/port_edit_comment', methods=['POST'])
@login_required
def port_edit_comment():
    request_strip = request.form["pk"].strip(':')
    ip = request.strip[0]
    port = request_strip[1]
    comment = request.form["value"]
    model.Port_service.update(comment=comment,
                              user_comment=current_user.get_login())\
        .where(model.Port_service.ip == ip,
               model.Port_service.port == port).execute()
    return json.dumps({})

@index_controller.route('/check_ip', methods=['POST'])
@login_required
def check_ip():
    id = request.form["id"]
    model.Alert_ip.update(is_checked=True,
                          checked_by=current_user.get_login())\
        .where(model.Alert_ip.id == id).execute()

    model.Alert_port.update(is_checked=True,
                            checked_by=current_user.get_login())\
        .where(model.Alert_port.alert_ip == id).execute()
    return json.dumps({})

@index_controller.route('/check_port', methods=['POST'])
@login_required
def check_port():
    id = request.form["id"]
    model.Alert_port.update(is_checked=True,
                            checked_by=current_user.get_login())\
        .where(model.Alert_port.id == id).execute()
    ip = model.Alert_port.select(model.Alert_port.alert_ip).where(model.Alert_port.id == id)
    if model.Alert_port.select().where(model.Alert_port.alert_ip == ip, ~model.Alert_port.is_checked).count() == 0:
        model.Alert_ip.update(is_checked=True).where(model.Alert_ip.id == ip).execute()
    return json.dumps({'username': current_user.get_login()})

@index_controller.route('/add_filter_ip' , methods=['POST'])
@login_required
def add_filter_ip():
    ip = request.form["ip"]
    if request.form["remove"]:
        model.Alert_port.delete().where(model.Alert_port.alert_ip == ip).execute()
        model.Alert_ip.delete().where(model.Alert_ip.ip == ip).execute()
    model.Rule_filter.insert(ip=ip, port=None).execute()
    return json.dumps({})

@index_controller.route('/add_filter_port', methods=['POST'])
@login_required
def add_filter_port():
    ip = request.form["ip"]
    port = int(request.form["port"])
    if request.form["remove"]:
        model.Alert_port.delete().where(model.Alert_port.alert_ip == ip, model.Alert_port.port == port).execute()
    model.Rule_filter.insert(ip=ip, port=port).execute()
    return json.dumps({})
