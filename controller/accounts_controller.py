from flask import Blueprint, render_template, request
import json
from flask.ext.login import login_required, current_user
import database.webdata as webdata
from database.model import *
from flask import redirect
import hashlib
import re

accounts_controller = Blueprint('accounts_controller', __name__)

#render page accounts
@accounts_controller.route('/')
@login_required
def accounts():
    if current_user.type == True:
        return render_template("views/accounts.html",
                               users=User.select(),
                               search=webdata.get_json_search())
    else:
        return redirect("/")

#add a new user
@accounts_controller.route('/add_user', methods=['POST'])
@login_required
def add_user():
    username = request.form["username"]
    password = request.form["password"]
    first_name = request.form["first_name"]
    last_name = request.form["last_name"]
    email = request.form["email"]
    #check if field are not empty
    if not username or not password or not first_name or not last_name or not email:
        return json.dumps({"status": "field"})
    else:
        #check if a vaild email address
        if CheckEmail(email):
            return json.dumps({"status": "email"})
        else:
            #check if password is strong
            if webdata.CheckPassword(password):
                id = User.insert(login=username, password=hashlib.sha1(password.encode()).hexdigest(), first_name=first_name, last_name=last_name, email=email, active=True).execute()
                return json.dumps({"status": "add","username": username, "first_name": first_name, "last_name": last_name, "email": email, "id": id})
            else:
                return json.dumps({"status": "check-password-failed"})

#change password
@accounts_controller.route('/change_password', methods=['POST'])
@login_required
def change_password():
    password = request.form["password"]
    login = request.form["login"]
    return webdata.change_password(login, password)

#change user name
@accounts_controller.route("/user_login", methods=['POST'])
@login_required
def user_login():
    login = request.form["value"]
    id = request.form['pk']
    return webdata.user_login(id, login)

#change first_name
@accounts_controller.route("/user_first_name", methods=['POST'])
@login_required
def user_first_name():
    first_name = request.form["value"]
    id = request.form['pk']
    return webdata.user_first_name(id, first_name)

#change last_name
@accounts_controller.route("/user_last_name", methods=['POST'])
@login_required
def user_last_name():
    last_name = request.form["value"]
    id = request.form['pk']
    return webdata.user_last_name(id, last_name)

#change email
@accounts_controller.route("/user_email", methods=['POST'])
@login_required
def user_email():
    email = request.form["value"]
    id = request.form['pk']
    return webdata.user_email(id, email)

#change status of the account - active or disabeld
@accounts_controller.route("/change_status",  methods=['POST'])
@login_required
def change_status():
    id = request.form['id']
    status = request.form["status"]
    if Checkadmin(id):
         return json.dumps({"status": "atleastoneadmin"})
    else:
        if status == "active":
            User.update(active=True).where(User.id == id).execute()
        else:
            User.update(active=False).where(User.id == id).execute()
        return json.dumps({"status": status})

#change account type - admin or user
@accounts_controller.route("/change_type",  methods=['POST'])
@login_required
def change_type():
    id = request.form['id']
    type = request.form["type"]
    if Checkadmin(id):
         return json.dumps({"type": "atleastoneadmin"})
    else:
        if type == "admin":
            User.update(type=True).where(User.id == id).execute()
        else:
            User.update(type=False).where(User.id == id).execute()
        return json.dumps({"type": type})

#check if it is realy a email addresse
@login_required
def CheckEmail(email):
    try:
        emailitems = email.rsplit('@', 1)
        emailitems.extend(emailitems[1].rsplit('.', 1))
    except:
        return True
    if [x for x in emailitems if not x.replace(".","").isalnum()]:
        return True
    else:
        return False

#check if atleast one admin account is present
@login_required
def Checkadmin(id):
    if User.select().where(User.active==1, User.type==1).count() == 1 and User.select().where(User.id==id,User.active==1, User.type==1).count() == 1:
        return True
    return False

