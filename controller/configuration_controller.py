from flask import Blueprint, render_template, request
import json
from flask.ext.login import login_required, current_user
from controller.scan_controller import __send_request__
import database.webdata as webdata
from database.model import *

configuration_controller = Blueprint('configuration_controller', __name__)

@configuration_controller.route('/')
@login_required
def configuration():
        settings = Scan_settings.select()\
            .where(Scan_settings.id == 1)\
            .join(Networks, 'left outer', on=Networks.scan == Scan_settings.id).aggregate_rows()
        threads = 0
        option = ""
        networks = None
        for setting in settings:
            threads = setting.threads
            option = setting.option
            networks = setting.networks
        return render_template("views/configuration.html",
                               threads=threads,
                               option=option,
                               networks=networks.select().order_by(Networks.network.asc()),
                               users=User.select(),
                               search=webdata.get_json_search(),
                               list_network=History_networks.select(),
                               scanners=Scanners.select())


@configuration_controller.route('/change_threads', methods=['POST'])
@login_required
def change_threads():
        threads = request.form["value"]
        Scan_settings.update(threads=threads).where(Scan_settings.id == 1).execute()
        return json.dumps({})


@configuration_controller.route('/change_option', methods=['POST'])
@login_required
def change_option():
    option = request.form["value"]
    Scan_settings.update(option=option).where(Scan_settings.id == 1).execute()
    return json.dumps({})

@configuration_controller.route('/add_address', methods=['POST'])
@login_required
def add_address():
    network = request.form["address"]
    if not network:
        return json.dumps({"status": "field"})
    try:
        id=Networks.insert(network = network, scan = 1).execute()
    except:
        return json.dumps({"status":"fail"})
    return json.dumps({"status":"add", "network":network, "id":id })

class Scanner:
    def __init__(self, uri, key):
        self.uri=uri
        self.key=key

@configuration_controller.route('/add_scanner', methods=['POST'])
@login_required
def add_scanner():
    key = request.form["key"]
    uri = request.form["address"].replace("http://","").replace("https://","").replace("/","")
    if not key or not uri:
        return json.dumps({"status": "field"})
    if (not ":" in uri):
        uri = uri + ":9630"
    data = __send_request__(Scanner(uri, key), "")
    if data and "Key Not Valid" not in data["status"] and uri and key:
        Scanners.insert(uri = uri, key = key).execute()
        return json.dumps({"success":True})
    else:
        return json.dumps({"success":False})

@configuration_controller.route('/remove_address', methods=['POST'])
@login_required
def remove_address():
    id = request.form["id"]
    Networks.delete().where(Networks.id == id).execute()
    return json.dumps({})

@configuration_controller.route("/user_login", methods=['POST'])
@login_required
def user_login():
    id = request.form['pk']
    login = request.form["value"]
    if len(login) > 3:
        User.update(login=request.form["value"]).where(User.id == id).execute()
        return json.dumps({})
    return json.dumps({})





