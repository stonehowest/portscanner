from flask import Blueprint, render_template, request
from peewee import fn
import database.model as model
import database.webdata as webdata
import json, re, netaddr, datetime
from flask.ext.login import login_required, current_user
from flask.ext.paginate import Pagination

server_controller = Blueprint('server_controller', __name__)

@server_controller.route('/')
@login_required
def server():
    search = False
    q = request.args.get('q')
    if q:
        search = True
    try:
        page = int(request.args.get('page',1))
    except ValueError:
        page = 1
    network_search = request.args.get("network")
    if network_search:
        if not re.match("[a-zA-Z]+", network_search):
            ips = list(netaddr.IPNetwork(network_search))
            data = model.Hostname_Ip.select(model.Port_service,
                                            model.Hostname_Ip,
                                            fn.COUNT(model.Port_service.port).alias("number"))\
                .where(model.Hostname_Ip.ip << ips).join(model.Port_service, on=model.Port_service.ip == model.Hostname_Ip.ip)\
                .paginate(page, 30).group_by(model.Hostname_Ip.ip).aggregate_rows()
            total = model.Hostname_Ip.select().where(model.Hostname_Ip.ip << ips).count()
        else:
            data = model.Hostname_Ip.select(model.Port_service,
                                            model.Hostname_Ip,
                                            fn.COUNT(model.Port_service.port).alias("number"))\
                .where(model.Hostname_Ip.hostname % network_search)\
                .join(model.Port_service, on=model.Port_service.ip == model.Hostname_Ip.ip)\
                .paginate(page,30)\
                .group_by(model.Hostname_Ip.ip)\
                .aggregate_rows()

            total = 1
    else:
        data = model.Hostname_Ip.select(model.Port_service,
                                        model.Hostname_Ip,
                                        fn.COUNT(model.Port_service.port).alias("number"))\
            .join(model.Port_service, "left outer", on=model.Port_service.ip == model.Hostname_Ip.ip)\
            .paginate(page, 30)\
            .group_by(model.Hostname_Ip.ip)\
            .aggregate_rows()
        total = model.Hostname_Ip.select().count()
    pagination = Pagination(page=page,
                            css_framework="bootstrap3",
                            perPage=30,
                            total=int(total/3),
                            search=search,
                            record_name='servers')
    return render_template("views/servers.html",
                           servers=data,
                           pagination=pagination,
                           network_filter=model.History_networks.select(),
                           now=datetime.datetime.now(),
                           search=webdata.get_json_search(), network=network_search)

# update server fields
@server_controller.route('/edit_comment', methods=['POST'])
@login_required
def server_edit_comment():
    ip = request.form["pk"]
    comment = request.form["value"]
    model.Hostname_Ip.update(comment=comment,
                             user_comment=current_user.get_login())\
        .where(model.Hostname_Ip.ip == ip).execute()
    return json.dumps({"user": current_user.get_login()})

#comment nog toetevoegen!
@server_controller.route('/port_edit_comment', methods=['POST'])
@login_required
def port_edit_comment():
    request_strip = request.form["pk"].split(':')
    ip = request_strip[0]
    port = request_strip[1]
    comment = request.form["value"]
    model.Port_service.update(comment=comment,
                              user_comment=current_user.get_login())\
        .where(model.Port_service.ip == ip, model.Port_service.port == port).execute()
    return json.dumps({"user": current_user.get_login()})

@server_controller.route('/get_history', methods=['POST'])
@login_required
def get_history():
    ip = request.form['ip']
    port = request.form['port']
    protocol = request.form['protocol']
    history = model.Port_service_history.select()\
        .where(model.Port_service_history.ip == ip,
               model.Port_service_history.protocol == protocol,
               model.Port_service_history.port == port)
    history_html = render_template("views/port-history.html",
                                   ports = history,
                                   now= datetime.datetime.now())
    return history_html

