from flask import Blueprint, render_template, request
from database.model import *
from flask.ext.login import login_required, current_user
import json

feedback_controller = Blueprint('feedback_controller', __name__)

@feedback_controller.route('/')
@login_required
def feedback():
    return render_template("views/feedback.html", user=current_user, feedback=Feedback.select().where(Feedback.is_fixed != 1).order_by(Feedback.date.desc()))

@feedback_controller.route('/send_feedback', methods=['POST'])
@login_required
def send_feedback():
    if not request.form["feedback"]:
        return json.dumps({"status": "field"})
    Feedback.insert(comment=request.form["feedback"], user = current_user.get_login()).execute()
    return json.dumps({"status": "add"})

@feedback_controller.route('/hide_feedback', methods=['POST'])
@login_required
def hide_feedback():
    Feedback.update(is_fixed = 1).where(Feedback.id == request.form["id"]).execute()
    return json.dumps({})
