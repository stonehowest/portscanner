from flask import Blueprint, render_template, request
from flask.ext.login import login_required, current_user
import database.webdata as data
import json
from database.model import *
import netaddr
from flask.ext.paginate import Pagination

filter_controller = Blueprint('filter_controller', __name__)
auth = Blueprint

@filter_controller.route('/')
@login_required
def filter():
    search = False
    q = request.args.get('q')
    if q:
        search = True
    try:
        page = int(request.args.get('page', 1))
    except ValueError:
        page = 1
    network = request.args.get("network")
    port = request.args.get("port")
    service = request.args.get("service")
    protocol = request.args.get("protocol")
    if port or service or protocol or network:
        if port:
            filters = Rule_filter.select().where(Rule_filter.port == port).paginate(page, 30)
            total = Rule_filter.select().where(Rule_filter.port == port).count()
        if service:
            filters = Rule_filter.select().where(Rule_filter.service % service).paginate(page, 30)
            total = Rule_filter.select().where(Rule_filter.service % service).count()
        if protocol:
            filters = Rule_filter.select().where(Rule_filter.protocol % protocol).paginate(page, 30)
            total = Rule_filter.select().where(Rule_filter.protocol % protocol).count()
        if network:
            filters = Rule_filter.select().where(Rule_filter.ip == network).paginate(page, 30)
            total = Rule_filter.select().where(Rule_filter.ip == network).count()
    else:
        filters = Rule_filter.select().paginate(page, 30)
        total = Rule_filter.select().count()
    search_json = []
    for row in Rule_filter.select(Rule_filter.port,Rule_filter.service, Rule_filter.protocol, Rule_filter.ip):
        if row.ip not in search_json:
            search_json.append(row.ip)
        if row.port not in search_json:
            search_json.append(row.port)
        if row.service not in search_json:
            search_json.append(row.service)
        if row.protocol not in search_json:
            search_json.append(row.protocol)
    pagination = Pagination(page=page,
                            css_framework="bootstrap3",
                            perPage=30,
                            total=int(total/3),
                            search=search,
                            record_name='filters')
    return render_template("views/filters.html",
                           filters=filters,
                           pagination=pagination,
                           search=json.dumps(search_json))

@filter_controller.route('/add', methods=['POST'])
@login_required
def add_filter():
    network = request.form['ip']
    port = request.form['port']
    protocol = request.form['protocol']
    service = request.form['service']
    comment = request.form['comment']
    if not network:
        return json.dumps({"status": "field"})
    else:
        portcheck = port
        if portcheck:
            portcheck = int(portcheck)
            if portcheck<0:
                return json.dumps({"status": "filter-underzero"})
        if not service:
            fil_service = "%"
        else:
            fil_service = service
        if not protocol:
            fil_protocol = '%'
        else:
            fil_protocol = protocol
        if network:
            try:
                ips = list(netaddr.IPNetwork(network))
            except netaddr.AddrFormatError:
                return json.dumps({"status": "filter-ip"})
            if not port:
                alert_ip = Alert_ip.select(Alert_ip.id).where(Alert_ip.ip << ips)
                if protocol or service:
                    alerts = Alert_port.select().where(Alert_port.alert_ip << alert_ip, Alert_port.service % fil_service, Alert_port.protocol % fil_protocol)
                    for alert in alerts:
                        Alert_port.delete().where(Alert_port.id == alert.id).execute()
                else:
                    for ip in alert_ip:
                        Alert_ip.delete().where(Alert_ip.id == ip.id).execute()
            else:
                alert_ip = Alert_ip.select(Alert_ip.id).where(Alert_ip.ip << ips)
                alerts = Alert_port.select().where(Alert_port.alert_ip << alert_ip,Alert_port.port == port, Alert_port.service % fil_service, Alert_port.protocol % fil_protocol)
                for alert in alerts:
                    Alert_port.delete().where(Alert_port.id == alert.id).execute()
        else:
            if not port:
                alerts = Alert_port.select(Alert_port.id).where(Alert_port.service % fil_service, Alert_port.protocol % fil_protocol)
                for alert in alerts:
                        Alert_port.delete().where(Alert_port.id == alert.id).execute()
                else:
                    alerts = Alert_port.select(Alert_port.id).where(Alert_port.service % fil_service, Alert_port.port == port, Alert_port.protocol % fil_protocol)
                    for alert in alerts:
                        Alert_port.delete().where(Alert_port.id == alert.id).execute()
    return data.insert_filter(network, service, port, comment, protocol)

@filter_controller.route('/delete', methods=['POST'])
@login_required
def delete():
    id = request.form['id']
    return data.remove_filter(id)

#update fields
@filter_controller.route('/edit_ip', methods=['POST'])
@login_required
def change_ip():
    id = request.form['pk']
    ip = request.form["value"]
    Rule_filter.update(ip=ip).where(Rule_filter.id == id).execute()
    return json.dumps({})

@filter_controller.route('/edit_port', methods=['POST'])
@login_required
def edit_port():
    id = request.form['pk']
    port = request.form["value"]
    Rule_filter.update(port=port).where(Rule_filter.id == id).execute()
    return json.dumps({})

@filter_controller.route('/edit_service', methods=['POST'])
@login_required
def edit_service():
    id = request.form['pk']
    service = request.form["value"]
    Rule_filter.update(service=service).where(Rule_filter.id == id).execute()
    return json.dumps({})

@filter_controller.route('/edit_comment', methods=['POST'])
@login_required
def edit_comment():
    id = request.form['pk']
    comment = request.form["value"]
    Rule_filter.update(comment=comment, user_comment=current_user.get_login())\
        .where(Rule_filter.id == id).execute()
    return json.dumps({"user": current_user.get_login()})
