$(document).ready(function(){

    $("#btn-add-network").on("click", function(){
        address = $("#network-address").val();
        $.ajax({
            type: "POST",
            data: { "address": address },
            url: "/configuration/add_address"
        }).success(function( msg ) {
            msg = JSON.parse(msg);
            if (msg.status == "add") {
                $("#network-add").show();
                setTimeout(function() { $("#network-add").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
            }
            else if (msg.status == "fail") {
                $("#network-fail").show();
                setTimeout(function() { $("#network-fail").fadeOut(); }, 5000);
            }
            else if(msg.status== "field"){
                $("#field").show();
                setTimeout(function() { $("#field").fadeOut(); }, 5000);
            }
        })
    });

    $("#btn-add-scanner").on("click", function(){
        address = $("#scanner-ip").val();
        key = $("#scanner-key").val();
        $.ajax({
            type: "POST",
            data: { "address": address, "key":key },
            url: "/configuration/add_scanner"
        }).success(function( msg ) {
            msg = JSON.parse(msg);
            if (msg.status == "add") {
                $("#scanner-add").show();
                setTimeout(function() { $("#scanner-add").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
            }
            else if (msg.status == "fail") {
                $("#scanner-fail").show();
                setTimeout(function() { $("#scanner-add").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
            }
            else if(msg.status== "field"){
                $("#field").show();
                setTimeout(function() { $("#field").fadeOut(); }, 5000);
            }
        });
    });

    $("#scan").on("click" , function(){
        var networks ="";
        $("#network-list").each( function(){
            networks += ($(this).text().trim()+" ");
        });
        var warning = "Are you sure you want to a full scan ";
        var button = $(this);
        if(button.attr("status") == "stop"){
            warning  ="Stop the scan"
        }
        if (confirm(warning) == true){
            $.ajax({
                type: "POST",
                url: "/scan/full_scan"
            }).success(function (msg) {
                if(button.attr("status") == "stop"){
                    button.attr("status", "start");
                    button.text("Full scan");
                }
                else{
                    button.attr("status", "stop");
                    button.text("Stop scan");
                }
            });
        }
    });

    $("#network-address").val("");
    $(".address").on("click",  removeNetwork);
    function removeNetwork(){
        var networkId = $(this).attr("id");
        var id = networkId.split("_")[1];
        if (confirm("Are you sure you want to delete "+$(this).text()) == true) {
            $.ajax({
                type: "POST",
                data: {"id": id},
                url: "/configuration/remove_address"
            }).success(function (msg) {
                $("#network_" + id).remove();
            });
            $("#network-address").val("")
        }
    }

    $(".remove").on("click", remove);
    function remove() {
        var networkId = $(this).attr("dataid");
        if (confirm("Are you sure you want to delete "+$(this).text()) == true) {
            $.ajax({
                type: "POST",
                data: {"id": networkId},
                url: "/configuration/remove_address"
            }).success(function (msg) {
                $("#network-removed").show();
                setTimeout(function() { $("#network-removed").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
            });
        }
    }

    $(".scannerremove").on("click", remove_scanner);
    function remove_scanner() {
        var scannerid = $(this).attr("dataid");
        if (confirm("Are you sure you want to delete "+$(this).text()) == true) {
            $.ajax({
                type: "POST",
                data: {"id": scannerid},
                url: "/configuration/remove_scanner"
            }).success(function (msg) {
                $("#scanner-removed").show();
                setTimeout(function() { $("#scanner-removed").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
            });
        }
    }

    $("#network-address").val("")
    $(".rescan_ip").on("click",function(){
        var ip = $(this).attr("ip");
        $.notify("Scan started on " + ip + "!", "info");
        $.ajax({
            type :"POST",
            dataType: "json",
            data: {"networks": ip},
            url : "/scan/scan",
        }).success(function(data) {
                if (data["success"]){
                    $.notify("Scan succeeded!", "succes");
                    var portdata = shit[0][2];
                }
                else
                    $.notify("Scan for IP " + ip + " failed!", "error");
        });
    });
});