$(document).ready(function(){

    // wanneer key enters in search field redirect with filtered
    $('input.typeahead').on('keyup', function(e) {
        if (e.which == 13) {
            redirectSearch($(this).val());
        }
    });

    $("#search-btn").on("click" ,function(){
        redirectSearch($("#search").val());
    });

    function redirectSearch(search){
        var formatLink = $(location).attr("href").split("?")[0];
        if(/^[0-9]+$/g.test(search)){
            redirect = (formatLink+"?port="+search)
        }
        else if(search == "tcp" || search == "udp"){
            redirect = (formatLink+"?protocol="+search)
        }
        else{
            redirect = (formatLink+"?service="+search)
        }
        $(location).attr("href", redirect)
    }

    // when a protocol is added it send its to the server and get a response back! It generates an error! Also if it worked it generates html code
    $("#send").on("click",function(){
        $.ajax({
            type: "POST",
            data: $("#input").serialize(),
            url: "add"
        }).success(function( msg ) {
            msg = JSON.parse(msg);
            if(msg.status =="fail"){
                $("#protocol-fail").show();
                setTimeout(function() { $("#protocol-fail").fadeOut(); }, 5000);
            }
            else if(msg.status =="fail-underzero"){
                $("#protocol-underzero").show();
                setTimeout(function() { $("#protocol-underzero").fadeOut(); }, 5000);
            }
            else if(msg.status== "field"){
                $("#field").show();
                setTimeout(function() { $("#field").fadeOut(); }, 5000);
            }
            else if (msg.status== "add"){
                $("#table-body").prepend("<tr id='" + msg.id + "'><td><a href='#' class='edit new" + msg.id + "' data-type='number' data-url='edit_port' data-pk='" + msg.id + "'>" + msg.port + "</a></td>" +
                "<td><a href='#' class='edit new" + msg.id + "' data-pk='" + msg.id + "' data-type='text' data-url='edit_protocol'>" + msg.protocol + "</a></td>" +
                "<td><a href='#' class='edit new" + msg.id + "' data-type='text' data-url='edit_service' data-pk='" + msg.id + "'  data-title='Change service'>" + msg.service + "</a></td>" +
                "<td><span class='user'>"+ msg.usercomment +"</span>: "+
                "<a href='#' class='edit comment new" + msg.id + "' data-type='text' data-url='edit_comment' data-pk='" + msg.id + "'  data-title='Change comment'>" + msg.comment + "</a></td>" +
                //"<td><span class='badge'>12</span></td>" +
                "<td><button type='button'' class='btn btn-default remove' port='"+ msg.port +"' protocol='"+ msg.protocol +"' aria-label='Left Align' dataid='" + msg.id + "' ><span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></td></tr>");
                $(".protocol-form").val("");
                $('.new' + msg.id).editable({
                    placement: "right",
                    error: function (errors) {
                    }
                });
                $(".remove").on("click", remove);
                $("#protocol-success").show();
                setTimeout(function() { $("#protocol-success").fadeOut(); }, 5000);
            }
        });
    });

    $(".remove").on("click", remove);
    // removing a row of protocol
    function remove() {
        var del_id = $(this).attr("dataid");
        var del_protocol = $(this).attr("protocol");
        var del_port = $(this).attr("port");
        var parent = $(this).parent().parent();
        if (confirm("Are you sure?") == true) {
            $.ajax({
                type: "POST",
                data: {"id": del_id},
                url: "delete"
            }).success(function (msg) {
                alert(del_port + " " + del_protocol + " deleted");
                parent.remove();
            });
        }
    }
});