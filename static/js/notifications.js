$(document).ready(function(){

    // when enter pressed in search field redirect
    $('input.typeahead').on('keyup', function(e) {
        if (e.which == 13) {
            redirectSearch($(this).val());
        }
    });

    $("#search-btn").on("click" ,function(){
        redirectSearch($("#search").val());
    });

    function redirectSearch(search){
        var formatLink = $(location).attr("href").split("?")[0];
        var redirect = (formatLink+"?network="+ search);
        $(location).attr("href", redirect);
    }

    // when an ip is checked
    $(".checked").on("click", function() {
        var id = $(this).attr("id");
        var button = $(this);
        if (confirm("Are you sure you want to allow this alert ?") == true) {
            $.ajax({
                type: "POST",
                data: { "id": id },
                url: "check_ip"
            }).success(function( msg ) {
                msg = JSON.parse(msg);
                $("#tr-header-"+id).removeClass("warning").addClass("success");
                succesCheck(msg.username , button);
                var ports = $(".notification_"+id).find(".port");
                ports.removeClass("warning").addClass("success");
                succesCheck(msg.username , $(".btn-port-"+id))
            });
        }
    });

    //set button disabled + tooltip
    function succesCheck(username , button){
        button.addClass('disabled');
        button.attr("data-title", "approved by "+ username);
        button.tooltip({position: "bottom"});
        button.off('click');
    }

    $(".add_filter_ip").on("click", function() {
        var ip = $(this).attr("ip");
        var ipId = $(this).attr("ip-id");
        var remove = false;
        if (confirm("Are you sure you want to delete all ?") == true) {
            remove = true
        }
        $.ajax({
            type: "POST",
            data: { "ip": ip, "remove": remove },
            url: "add_filter_ip"
        }).success(function( msg ) {
            if(remove){
                $(".notification_"+ipId).remove()
            }
        });
    });

    $(".checked_port").on("click", function() {
        var id = $(this).attr("id");
        var ipId = $(this).attr("ip-id");
        var button = $(this);
        if (confirm("Do you want to check this ports alert ?") == true){
            $.ajax({
                type: "POST",
                data: { "id": id },
                url: "check_port"
            }).success(function( msg ){
                var msgJson = JSON.parse(msg);
                button.parent().parent().removeClass("warning").addClass("success");
                succesCheck(msgJson.username , button);
                //if their are no warnings anymore then also the ip needs to be checked
                if(!$("#tr-"+ipId).find(".warning").length > 0){
                    $("#tr-header-"+ipId).removeClass("warning").addClass("success");
                    succesCheck("system" , $("#"+ipId));
                }
            });
        }
    });

    $(".filter_port").on("click", function() {
        var ip = $(this).attr("ip");
        var port = $(this).attr("port");
        var ipId = $(this).attr("ip-id");
        var parent = $(this).parent().parent();
        var remove = false;
        if (confirm("Do you want to remove existing records ?") == true){
            remove = true
        }
        $.ajax({
            type: "POST",
            data: { "ip": ip, "port":port, 'ipId': ipId, "remove":remove },
            url: "add_filter_port"
        }).success(function( msg ) {
            if(remove){
                parent.remove();
                $(".number_"+ipId).text($(".notification_"+ipId).find(".port").length);
            }
        });
    });
});
