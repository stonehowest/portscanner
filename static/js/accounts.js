$(document).ready(function(){

    //actief or deactivate a account
    $(".btn-status").on("click", function(){
        var button  = $(this);
        $.ajax({
            type: "POST",
            data: { "status": button.attr("status") , "id":button.attr("user-id")},
            url: "/accounts/change_status"
        }).success(function( msg ) {
            msg = JSON.parse(msg);
            if (msg.status == "active") {
                button.attr("status", "disabled");
                button.text("disabled");
                $("#user-status").show();
                setTimeout(function() { $("#user-status").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
            }
            else if (msg.status == "atleastoneadmin") {
                button.attr("status", "disabled");
                button.text("disabled");
                $("#user-failed-type").show();
                setTimeout(function() { $("#user-failed-type").fadeOut(); }, 5000);
            }
            else if (msg.status == "disabled") {
                button.attr("status", "active");
                button.text("active");
                $("#user-status").show();
                setTimeout(function() { $("#user-status").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
            }
        });
    });

    //change the type of the account
    $(".btn-type").on("click", function(){
    var button  = $(this);
        $.ajax({
            type: "POST",
            data: { "type": button.attr("type") , "id":button.attr("user-id")},
            url: "/accounts/change_type"
        }).success(function( msg ) {
            msg = JSON.parse(msg);
            if (msg.type == "user") {
                button.attr("type", "admin");
                button.text("admin");
                $("#user-type").show();
                setTimeout(function() { $("#user-type").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
            }
            else if (msg.type == "atleastoneadmin") {
                button.attr("type", "user");
                button.text("user");
                $("#user-failed-type").show();
                setTimeout(function() { $("#user-failed-type").fadeOut(); }, 5000);
            }
            else if (msg.type == "admin"){
                button.attr("type", "user");
                button.text("user");
                $("#user-type").show();
                setTimeout(function() { $("#user-type").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
            }
        });
    });

    //add user
    $("#btn-add-user").on("click", function(){
        $.ajax({
            type: "POST",
            data: { "username": $("#username").val(), "password": $("#password").val(), "first_name": $("#first_name").val(), "last_name": $("#last_name").val(), "email": $("#email").val() },
            url: "/accounts/add_user"
            }).success(function( msg ) {
            msg = JSON.parse(msg);
            if (msg.status == "field") {
                $("#user-field").show();
                setTimeout(function() { $("#user-field").fadeOut(); }, 5000);
            }
            else if (msg.status == "email") {
                $("#user-email").show();
                setTimeout(function() { $("#user-email").fadeOut(); }, 5000);
            }
            else if (msg.status == "check-password-failed") {
                $("#check-password-failed").show();
                setTimeout(function() { $("#check-password-failed").fadeOut(); }, 5000);
            }
            else if (msg.status == "add") {
                $("#user-added").show();
                setTimeout(function() { $("#user-added").fadeOut(); }, 5000);
                setTimeout(function() { location.reload(); }, 5000);
                var tr = "<tr><td><a href='#' class='edit new-"+msg.username+"' data-type='text' data-url='user_login'  data-pk='"+msg.id+"'  data-title='Change comment'>"+msg.username+"</a></td><td><a href='#' class='edit new-"+msg.first_name+"' data-type='text' data-url='user_first_name' data-pk='"+msg.id+"' data-title='Change comment'>"+msg.first_name+"</a></td><td><a href='#' class='edit new-"+msg.last_name+"' data-type='text' data-url='user_last_name' data-pk='"+msg.id+"'  data-title='Change comment'>"+msg.last_name+"</a></td><td><a href='#' class='edit new-"+msg.email+"' data-type='text' data-url='user_email' data-pk='"+msg.id+"'  data-title='Change comment'>"+msg.email+"</a></td><td>Change user from active to <button user-id='"+msg.id+"' status='disabled'  class='btn btn-primary btn-xs btn-status'>disabled</button></td><td>Change account type from user to <button user-id='"+msg.id+"' type='admin' class='btn btn-primary btn-xs btn-type'>admin</button></td></tr>";
                $("#user-list").append(tr);
                $('.new-'+msg.id).editable({
                    placement: "right",
                    error: function (errors) {
                    }
                })
            }
            }).error(function() {
                $("#user-fail").show();
                setTimeout(function() { $("#user-fail").fadeOut(); }, 5000);
        });
    });

    //change password
    $("#btn-change-password").on("click", function(){
        $.ajax({
            type: "POST",
            data: { "password": $("#new-password").val(), "login": $("#login-password").val()},
            url: "/accounts/change_password"
        }).success(function( msg ) {
            msg = JSON.parse(msg);
            if (msg.status == "field") {
                $("#password-field").show();
                setTimeout(function() { $("#password-field").fadeOut(); }, 5000);
            }
            else if (msg.status == "check-password-failed") {
                $("#check-password-failed").show();
                setTimeout(function() { $("#check-password-failed").fadeOut(); }, 5000);
            }
            else if(msg.status == "changed"){
                $("#password-success").show();
                setTimeout(function() { $("#password-success").fadeOut(); }, 5000);
            }
            else if(msg.status == "failed"){
                $("#password-fail").show();
                setTimeout(function() { $("#password-fail").fadeOut(); }, 5000);
            }
        });
    });
});