$(document).ready(function(){

    $("#btn-change-password").on("click", function(){
        $.ajax({
            type: "POST",
            data: { "old": $("#old-password").val(), "new": $("#new-password").val() },
            url: "/profile/change_password"
        }).success(function( msg ) {
            msg = JSON.parse(msg);
            if(msg.status== "changed"){
                $("#password-success").show();
                setTimeout(function() { $("#password-success").fadeOut(); }, 5000);
            }
            else if (msg.status == "field"){
                $("#field").show();
                setTimeout(function() { $("#field").fadeOut(); }, 5000);
            }
            else if (msg.status == "check-password-failed"){
                $("#check-password-failed").show();
                setTimeout(function() { $("#check-password-failed").fadeOut(); }, 5000);
            }
            else if (msg.status == "password-fail"){
                $("#password-fail").show();
                setTimeout(function() { $("#password-fail").fadeOut(); }, 5000);
            }
        });
    });
});