$(document).ready(function(){

    // when enter pressed in search field redirect
    $('input.typeahead').on('keyup', function(e) {
        if (e.which == 13) {
            var search = $(this).val();
            redirectSearch(search);
        }
    });

    function redirectSearch(search) {
        var formatLink = $(location).attr("href").split("?")[0];
        var patternIp = /^(([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5]).){3}([0-9]|[1-9][0-9]|1[0-9]{2}|2[0-4][0-9]|25[0-5])/g;
        //checks welke data er behandeld moet worden
        if(patternIp.test(search)) {
            redirect = (formatLink+"?network="+search)
        }
        else if(/^[0-9]+$/g.test(search)){
            redirect = (formatLink+"?port="+search)
        }
        else if(search == "tcp" || search == "udp"){
            redirect = (formatLink+"?protocol="+search)
        }
        else{
            redirect = (formatLink+"?service="+search)
        }
        $(location).attr("href", redirect)
    }

    $("#search-btn").on("click" ,function(){
        redirectSearch($("#search").val());
    });

    function remove() {
        var parent = $(this).parent().parent();
        if (confirm("Are you sure?") == true) {
            $.ajax({
                type: "POST",
                data: {"id": parent.attr("id")},
                url: "delete"
            }).success(function (msg) {
                parent.remove();
            });
        }
    }

    $(".remove").on("click",remove );
    $("#send").on("click",function(){
        $.ajax({
            type: "POST",
            data: $("#input").serialize(),
            url: "add"
        }).success(function( msg ) {
            msg = JSON.parse(msg);
            if(msg.status== "filter-underzero"){
                $("#filter-underzero").show();
                setTimeout(function() { $("#filter-underzero").fadeOut(); }, 5000);
            }
            else if(msg.status== "filter-ip"){
                $("#filter-ip").show();
                setTimeout(function() { $("#filter-ip").fadeOut(); }, 5000);
            }
            else if(msg.status== "field"){
                $("#field").show();
                setTimeout(function() { $("#field").fadeOut(); }, 5000);
            }
            else{
                $(".empty").val("");
                $("#table-body").append("<tr id='"+msg.id+"'><td><a href='#' class='edit new"+msg.id+"' data-emptytext='****'  data-type='text' data-url='edit_ip' data-pk='"+msg.id+"'  data-title='Change comment'>"+msg.ip+"</a></td>" +
                "<td><a href='#' class='edit new"+msg.id+"'data-emptytext='****' data-type='text' data-url='edit_ip' data-pk='"+msg.id+"'  data-title='Change comment'>"+msg.port+"</a></td>" +
                "<td><a href='#' class='edit new"+msg.id+"'data-emptytext='****' data-type='text' data-url='edit_ip' data-pk='"+msg.id+"'  data-title='Change comment'>"+msg.service+"</a></td>" +
                "<td><a href='#' class='edit new"+msg.id+"'data-emptytext='****' data-type='text' data-url='edit_ip' data-pk='"+msg.id+"'  data-title='Change comment'>"+msg.protocol+"</a></td>" +
                "<td><a href='#' class='edit new"+msg.id+"'data-emptytext='****' data-type='text' data-url='edit_ip' data-pk='"+msg.id+"' data-title='Change comment'>"+msg.comment+"</a></td>" +
                "<td><button type='button' class='btn btn-default' aria-label='Left Align' id='button-"+msg.id+"'>" + "<span class='glyphicon glyphicon-trash' aria-hidden='true'></span></button></td></tr>");
                $("#button-"+msg.id).on("click",remove);
                $('.new'+msg.id).editable({
                    placement: "right",
                    error: function (errors) {
                    }
                });
            }
        });
    });
});
