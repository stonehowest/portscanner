$(document).ready(function() {

    //search field
    $(".tt-input").on("change", function(){
        $.each($('.tt-suggestion'), function(key, value){
            if($(value).text() == $(this).text()){
                $(value).addClass("tt-suggestion-active")
            }
        });
    });

    var substringMatcher = function(strs) {
        return function findMatches(q, cb) {
            var matches, substringRegex;
            // an array that will be populated with substring matches
            matches = [];
            // regex used to determine if a string contains the substring `q`
            var substrRegex = new RegExp(q, 'i');
            // iterate through the pool of strings and for any string that
            // contains the substring `q`, add it to the `matches` array
            $.each(strs, function(i, str) {
                if (substrRegex.test(str)) {
                    matches.push(str);
                }
            });
            cb(matches);
        };
    };

    if($("#source-search").length) {
        var states = substringMatcher(JSON.parse($("#source-search").val()));
    }

    //setting sources of the search field
    $('.typeahead').typeahead({
        hint: true,
        highlight: true,
        minLength: 1
    },{
        name: 'ip',
        source: states
    });

    //for disabled buttons making a tooltip
    $('.disabled').tooltip({position: "bottom"});

    // check if their is a scan
    //if so then update theloadbar
    /*function controle(){
        $.ajax({
            type: "POST",
            url: "" +
            "../configuration/get_scan_progress"
        })
            .success(function( msg ) {
                msg = JSON.parse(msg);
                if(msg.on) {

                    if(!$("#location_progressbar").is(":visible"))
                    {
                        $("#location_progressbar").show()
                    }
                    var load = parseInt(msg.load);
                    $("#progress-bar").text(load + "%").attr("aria-valuenow", load).css("width", load + "%");
                }
                else
                {

                    if($("#location_progressbar").is(":visible"))
                    {
                        $("#location_progressbar").hide()
                    }
                }
            });

    }
    setInterval(controle,3000);*/

    // redirect if the network filter changes
    $("#filter").on("change",function(){
        var formatLink = $(location).attr("href").split("?")[0];
        var redirect = (formatLink+"?network="+$(this).val());
        $(location).attr("href", redirect)
    });

    //setting the editable
    $.fn.editable.defaults.mode = 'inline';
    $('.edit').editable({
        placement: "right",
        ajaxOptions: {
            dataType: 'json'
        },
        error: function (errors) {
        },success: function(response){
            var element = $(this);
            if(element.hasClass("comment")){
                element.siblings("span.user").text(response.user+": ");
            }
        }
    });
});

