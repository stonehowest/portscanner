$(document).ready(function(){

    $("#btn-send").on('click', function(){
        $.ajax({
            type: "POST",
            data: { "feedback": $("#feedback").val() },
            url: "/feedback/send_feedback"
        }).success(function( msg ) {
            msg = JSON.parse(msg);
            if(msg.status== "field"){
                $("#field").show();
                setTimeout(function() { $("#field").fadeOut(); }, 5000);
            }
            if (msg.status== "add"){
            setTimeout(function() { location.reload(); }, 50);
            }
        });
    });

    $(".btn-hide").on("click", function(){
        $.ajax({
            type: "POST",
            data: { "id":$(this).attr("feedback-id")},
            url: "/feedback/hide_feedback"
        }).success(function() {
            setTimeout(function() { location.reload(); }, 50);
        });
    });
});