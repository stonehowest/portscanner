$(document).ready(function(){

    $('input.typeahead').on('keyup', function(e) {
        if (e.which == 13) {
            $(location).attr("href", ($(location).attr("href").split("?")[0]+"?network="+$(this).val()))
        }
    });

    $("#search-btn").on("click" ,function(){
        $(location).attr("href", ($(location).attr("href").split("?")[0]+"?network="+$("#search").val()))
    });

    $(".checked").on("click", function() {
        var id = $(this).attr("id");
        if (confirm("Are you sure you want to allow this alert ?") == true) {
            $.ajax({
                type: "POST",
                data: { "id": id },
                url: "notifications/check_ip"
            }).success(function( msg ) {
                $(".notification_"+id).remove()
            });
        }
    });

    $(".add_filter_ip").on("click", function() {
        var ip = $(this).attr("ip");
        var id = $(this).attr("id");
        var remove = false;
        if (confirm("Do you want to delete existing ports ?") == true) {
            remove = true
        }
        $.ajax({
            type: "POST",
            data: { "ip": ip , "remove" : remove },
            url: "notifications/add_filter_ip"
        }).success(function( msg ) {
            if(remove){
                $(".notification_"+id).remove();
            }
        });
    });

    $(".checked_port").on("click", function() {
        var id = $(this).attr("id");
        var parent = $(this).parent().parent();
        var ipId = $(this).attr("ip-id");
        var numberIpId = $("#number-" +ipId);
        if (confirm("Are you sure you want to allow this alert ?") == true) {
            $.ajax({
                type: "POST",
                data: { "id": id },
                url: "notifications/check_port"
            }).success(function( msg ) {
                numberIpId.text(parseInt(numberIpId.text())-1);
                parent.remove();
                //if their are no port left, delete alert ip also
                if(parseInt(numberIpId.text()) == 0){
                    $(".notification_"+ipId).remove();
                }
            });
        }
    });

    $(".filter_port").on("click", function() {
        var ip = $(this).attr("ip");
        var ipId = $(this).attr("ip-id");
        var port = $(this).attr("port");
        var parent = $(this).parent().parent();
        var remove = false;
        var numberIpId = parseInt($("#number-" +ipId));
        if (confirm("Do you want to delete existing ports ?") == true){
            remove = true
        }
        $.ajax({
            type: "POST",
            data: { "ip": ip,"ipId": ipId, "port":port, "remove": remove },
            url: "notifications/add_filter_port"
        }).success(function( msg ) {
            if(remove){
                numberIpId.text(parseInt(numberIpId.text())- 1);
                parent.remove();
                if(parseInt(numberIpId) == 0){
                    $(".notification_"+ipId).remove();
                }
            }
        });
    });

    $(".rescan_ip").on("click",function(){
        var ip = $(this).attr("ip");
        $.notify("Scan started on " + ip + "!", "info");
        $.ajax({
            type :"POST",
            dataType: "json",
            data: {"networks": ip},
            url : "scan/scan",
        }).success(function(data) {
                if (data["success"]){
                    $.notify("Scan succeeded!", "succes");
                    var portdata = shit[0][2];
                }
                else
                    $.notify("Scan for IP " + ip + " failed!", "error");
        });
    });
});
