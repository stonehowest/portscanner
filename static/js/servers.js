$(document).ready(function(){

    // wanneer er enter geklikt wordt op search veldje redirect het met een filter
    $('input.typeahead').on('keyup', function(e) {
        if (e.which == 13) {
            redirectSearch($(this).val())
        }
    });

    $("#search-btn").on("click" ,function(){
            redirectSearch($("#search").val());
    });

    function redirectSearch(search){
        var formatLink = $(location).attr("href").split("?")[0];
        var redirect = (formatLink+"?network="+search);
        $(location).attr("href", redirect)
    }

    $(".click-port-history").on("click",function(){
        //protocol ook nog filter
        var ele = $(this);
        if($("#history_port_"+ele.attr("port-id")).has("tr")) {
            appendToHistory(ele.attr("ip"), ele.attr("protocol"), ele.attr("port"), ele.attr("port-id"))
        }
    });

    function appendToHistory(ip,protocol, port, portId){
    $.ajax({
        type: "POST",
        data: { "ip": ip , "protocol":protocol, "port" : port },
        url: "get_history"
    }).success(function( msg ) {
        $("#history_port_"+portId).html(msg)
        });
    }

    $(".rescan_ip").on("click",function(){
        var ip = $(this).attr("ip");
        $.notify("Scan started on " + ip + "!", "info");
        $.ajax({
            type :"POST",
            dataType: "json",
            data: {"networks": ip},
            url : "/scan/scan",
        }).success(function(data) {
                if (data["success"]){
                    $.notify("Scan succeeded!", "succes");
                    var portdata = shit[0][2];
                }
                else
                    $.notify("Scan for IP " + ip + " failed!", "error");
        });
    });
});