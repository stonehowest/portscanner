from functools import wraps
import hashlib
import random
import threading
from flask import Flask, request
from flask import json
import configparser
import requests

import nmap
#87.238.160.0/21
#5.134.0.0/21

class ThreadPool:
    threads = []
    tasks = []
    lock = threading.Lock()
    starttasks = 0
    done  = 0
    class __worker__(threading.Thread):
        def __init__(self, pool):
            threading.Thread.__init__(self)
            self.pool = pool
        def run(self):
            try:
                while (self.pool.hasNextTask()):
                    task = self.pool.getNextTask()
                    task.run()
            except Exception as e: print( str(e))
            self.pool.threads.remove(self)
            if (not self.pool.isRunnning() and self.pool.callback is not None):
                self.pool.callback(task)
            print(self.pool.getProgress()*100)
            print("Worker ended")


    def __init__(self, maxThreads):
        self.maxThreads = maxThreads

    def start(self, callback=None):
        if not self.isRunnning():
            self.starttasks = len(self.tasks)
            print("starting threadpool with", self.maxThreads, "threads and", self.starttasks, "tasks" )
            for i in range(0, self.maxThreads if self.maxThreads < len(self.tasks) else len(self.tasks)):
                thread = self.__worker__(self)
                self.threads.append(thread)
                thread.start()
            self.callback = callback
            return
        raise Exception("Pool is already running")

    def join(self):
        for thread in self.threads:
            thread.join()

    def sumbit(self, task):
        for t in task:
            self.tasks.append(t)

    def hasNextTask(self):
        return len(self.tasks) != 0

    def getNextTask(self):
        self.lock.acquire()
        task = self.tasks[0]
        self.tasks.remove(task)
        self.lock.release()
        return task

    def getProgress(self):
        if self.starttasks == 0:
            return 0
        return 1-(len(self.threads) + len(self.tasks)) / self.starttasks

    def isRunnning(self):
        return len(self.threads) != 0


class ScanTask:
    def __init__(self, adres, setting, data):
        self.adres = adres
        self.setting = setting
        self.data = data

    def run(self):
        print("started for ", self.adres, self.setting)
        nm = nmap.PortScanner()
        nm.scan(self.adres, arguments=self.setting + " --dns-servers 8.8.8.8")
        if len(self.setting.split(" ")) == 2:
            protocols = ['tcp', 'udp']
        else:
            if self.setting == "-sT":
                protocols=['tcp']
            else:
                protocols=["udp"]
        if len(nm.all_hosts()) == 1:
            host_data = {}
            host = nm.all_hosts()[0]
            host_data["ip"] = host
            host_data["hostname"] = nm[host].hostname()
            new_servicess = []
            new_services=[]
            for proto in protocols:
                for port in nm[host][proto].keys():
                    new_service ={}
                    new_service["port"] = port
                    new_service["open"] = True
                    new_service["protocol"] = proto
                    new_service["name"] = nm[host][proto][port]["name"]
                    new_servicess.append(new_service)
                    print(host, proto)

            host_data["services"] = new_servicess
            self.data.append(host_data)
            return new_services;

class HttpTask:
    def __init__(self, addres, port, data):
        self.addres = addres
        self.port = port

    def run(self):
        req = requests.get(self.addres)
        self.data.append({"address":self.addres, "length":len(req.text)})

def __get_pool__():
    return ThreadPool(50)


app = Flask(__name__)
pool = None

def checkKey(f):
        @wraps(f)
        def wrapper(*args, **kwds):
            data = request.get_json()
            if not data or not data["key"] == key:
                return json.dumps({"status":"Key Not Valid",})
            return f(*args, **kwds)
        return wrapper

@app.route("/scan_async", methods=["POST"])
@checkKey
def scan_async():
    global pool
    try:
        scans, data = __get_scantasks__(request.form["addresses"])
        pool.sumbit(scans)
        pool.start()
        return json.dumps({"status":"Started"})
    except Exception as e: return json.dumps({"error":e})

def scan_async_callback(new_services):
    print(new_services)


@app.route("/scan", methods=["POST"])
@checkKey
def scan():
    global pool
    scans, data = __get_scantasks__(request.get_json()["addresses"])
    pool.sumbit(scans)
    try:
        pool.start()
    except Exception as e: return json.dumps({"error":e})
    pool.join()
    print("done!")
    return json.dumps({"data":data})

def __get_scantasks__(addresses):
    data = []
    print(addresses)
    scans = []
    for address in addresses:
        if (address):
                scans.append(ScanTask(str(address), "-sT", data))
    return scans, data;

def __get_httptasks__(addresses):
    data = []
    print(addresses)
    scans = []
    for address in addresses:
        if (address):
            d = address.split(":")
            scans.append(HttpTask(str(d[0], d[1]), data))
    return scans, data




@app.route('/', methods=["POST"])
@checkKey
def getstatus():
    return json.dumps({"status":"OK", "scanning": False if pool is None else pool.isRunnning(), "progress":pool.getProgress()});


key = hashlib.sha256(str(random.getrandbits(256)).encode('utf-8')).hexdigest()
host = "localhost"
port = 9630
debug = False

def loadConfig():
    global key, host, port, debug
    section = "Settings"
    config = configparser.ConfigParser()
    config.read("config.ini")
    if config.has_section(section):
        key = config.get(section, "key")
        host = config.get(section, "host")
        port = int(config.get(section, "port"))
        debug = bool(config.get(section, "debug"))
    else:
        cfgfile = open("config.ini",'w+')
        config.add_section(section)
        config.set(section, "key", key)
        config.set(section, "host", host)
        config.set(section, "port", str(port))
        config.set(section, "debug", str(debug))
        config.write(cfgfile)
        cfgfile.close()

if __name__ == '__main__':
    pool = __get_pool__()
    loadConfig()
    app.debug=debug
    app.run(host=host, port=port)
