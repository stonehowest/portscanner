from database.model import *
from netaddr import IPNetwork

warning_ip_down = "Ip went down"
warning_hostname_changed = "Hostname changed from {0}"
warning_ip_is_new = "Ip is new and went up"
warning_port_down = "Went down"
warning_port_up = "Went Up"
warning_port_is_new = "Port is new and went up"
ip_warning = "Changed elements in this Ip"
warning_change_service = "Changed service from {0}"
warning_change_service_new = "First service"
warning_service_wrong = "Other service than expected, expected {0}"

def __get_proc_dict__(proto):
    proto_dict =[]
    for protocol_row in proto:
        protocol = []
        protocol.append(protocol_row.port)
        protocol.append(protocol_row.protocol)
        protocol.append(protocol_row.service)
        proto_dict.append(protocol)
    return proto_dict

def __get_filter_dict__(rules):
    filter_dict = []
    for rule in rules:

        dict_rule = []
        dict_rule.append(rule.ip)
        dict_rule.append(rule.port)
        dict_rule.append(rule.protocol)
        dict_rule.append(rule.service)
        filter_dict.append(dict_rule)
    return filter_dict



def update_dicts():
    global proto_dict
    global filter_dict
    proto_dict = __get_proc_dict__(Protocol.select())
    filter_dict = __get_filter_dict__(Rule_filter.select())


def check_filter(ip, protocol, port, service):
    found = False
    for rule in filter_dict:
        if "\\" not in rule[0]:
            if (rule[0] == "" or rule[0] == ip) and (rule[1] == 0 or rule[1] == port) \
                    and (rule[2] == "" or rule[2] == protocol) and (rule[3] == "" or rule[3] == service):
                print(rule[0], ip, rule[1], port, rule[2], protocol, rule[3], service)
                found = True
        else:
            ip_list = list(IPNetwork(rule[0]))
            if(ip in ip_list) and (rule[1] == 0 or rule[1] == port) and (rule[2] == "" or rule[2] == protocol)\
                    and (rule[3] == "" or rule[3] == service):
                print(rule[0], ip, rule[1], port, rule[2], protocol, rule[3], service)
                found = True

    if found:
        return True
    else:
        return False


def alert(ip, message, protocol="", port=0, service=""):
    if not (check_filter(ip, protocol, port, service)):
        print(ip, port, message)
        try:
            ip_alert = Alert_ip.get(Alert_ip.ip == ip)
            if message == warning_ip_down or message == ip_warning:
                if not ip_alert.warning == message:
                    Alert_ip.update(is_checked=False, warning=message).where(Alert_ip.id == ip_alert.id,
                                                                              Alert_ip.is_checked).execute()

        except Alert_ip.DoesNotExist:
            ip_alert = Alert_ip.insert(ip=ip, warning=warning_ip_is_new).execute()
        "Make sub-alert"

        if port and protocol:
            try:
                port_alert = Alert_port.get(Alert_port.alert_ip == ip_alert,
                                            Alert_port.port == port,
                                            Alert_port.warning == message)

                Alert_port.update(is_checked=False).\
                    where(Alert_port.id == port_alert.id, Alert_port.is_checked).execute()

                Alert_ip.update(is_checked=False, changed=datetime.datetime.now())\
                    .where(Alert_ip.id == ip_alert).execute()

            except Alert_port.DoesNotExist:
                Alert_port.insert(port=port,protocol=protocol, warning=message, alert_ip=ip_alert,
                                  is_checked=False, service=service).execute()
                Alert_ip.update(is_checked=False, changed=datetime.datetime.now())\
                    .where(Alert_ip.id == ip_alert).execute()
                """print("toevoegen")"""

def get_old_services_in_dict(old_services):
    old_services_in_dict = []
    for old_service in old_services:
        old_servicess = {"ip":old_service.ip,
                         "hostname":old_service.hostname}
        old_ports =[]
        for old_p in old_service.ports:
            old_port = {"port":old_p.port,
                        "open":old_p.is_up,
                        "protocol":old_p.protocol,
                        "name":old_p.service,
                        "changed":old_p.changed}
            old_ports.append(old_port)
        old_servicess["services"] = old_ports
        old_servicess["changed"] = old_service.changed
        old_services_in_dict.append(old_servicess)
    return old_services_in_dict


def dict_ports(ports):
    ports_dict = []
    for port in ports:
        ports_dict.append(str(port["port"])+":"+port["protocol"])
    return ports_dict


def check_port_in_protocols(ip, port, proto_dict):
    found = False
    expected = []
    for protocol in proto_dict:
        if port["port"] == protocol[0] and port["protocol"] == protocol[1]:
            found = True
            print(protocol[1],port["protocol"],"lol",port["name"],protocol[2], port)
            #controle if already was controlled... else it generates an alert!
            if port["name"] != protocol[2]:
                expected.append(protocol[2])
    if not found:
        alert(ip=ip, port=port["port"], protocol=port["protocol"],
              message=warning_service_wrong.format("Unknown"), service=port["name"])

    if expected:
        alert(ip=ip, port=port["port"], protocol=port["protocol"],
              message=warning_service_wrong.format(str(expected)), service=port["name"])


def check_new_services(old_services, new_services):
    for new_service in new_services:
        found = False
        for old_service in old_services:
           if new_service["ip"] == old_service["ip"]:
                ports_in_ip = dict_ports(old_service["services"])
                found = True

                for new_port in new_service["services"]:
                    if not str(new_port["port"])+":"+new_port["protocol"] in ports_in_ip:
                        Port_service.insert(is_up=True, ip=new_service["ip"],
                                            port=new_port["port"], service=new_port["name"],
                                            protocol=new_port["protocol"]).execute()

                        Hostname_Ip.update(changed=datetime.datetime.now())\
                            .where(Hostname_Ip.ip == new_service["ip"]).execute()

                        alert(ip=new_service["ip"], message=warning_port_is_new,
                              port=new_port["port"], protocol=new_port["protocol"],
                              service=new_port["name"])

                        check_port_in_protocols(new_service["ip"], new_port, proto_dict)
        if not found:
            Hostname_Ip.insert(ip=new_service["ip"], hostname=new_service["hostname"]).execute()
            alert(ip=new_service["ip"], message=warning_ip_is_new )
            for port in new_service["services"]:
                Port_service.insert(is_up=True, ip=new_service["ip"], port=port["port"],
                                    service= port["name"], protocol = port["protocol"]).execute()

                Hostname_Ip.update(changed=datetime.datetime.now())\
                    .where(Hostname_Ip.ip == new_service["ip"]).execute()

                alert(ip= new_service["ip"], message=warning_port_is_new, port=port["port"],
                      protocol=port["protocol"], service=port["name"])

                check_port_in_protocols(new_service["ip"], port, proto_dict)


def check_old_services(old_services, new_services):
    for old_service in old_services:
            found_ip = False
            for new_service in new_services:
                if new_service["ip"] == old_service["ip"]:
                    found_ip = True
                    if not new_service["hostname"] == old_service["hostname"]:
                        #foreign key current service
                        Hostname_Ip_history.insert(ip=old_service["ip"],
                                                   hostname=old_service["hostname"],
                                                   changed=old_service["changed"]).execute()

                        Hostname_Ip.update(hostname=new_service["hostname"], changed=datetime.datetime.now())\
                            .where(Hostname_Ip.ip == new_service["ip"]).execute()

                        alert(ip=new_service["ip"],
                              message=warning_hostname_changed.format(old_service["hostname"]))

                    for old_port in old_service["services"]:
                        found_port = False
                        for new_port in new_service["services"]:
                            if old_port["port"] == new_port["port"] and \
                                            old_port["protocol"] == new_port["protocol"]:

                                found_port = True
                                if not old_port["open"]:
                                    Port_service_history.insert(ip=new_service["ip"],
                                                                port=old_port["port"],
                                                                service=old_port["name"],
                                                                is_up=old_port["open"],
                                                                protocol=old_port["protocol"],
                                                                changed=old_port["changed"]).execute()

                                    Port_service.update(is_up=True, changed=datetime.datetime.now())\
                                        .where(Port_service.ip == old_service["ip"],
                                               Port_service.protocol == old_port["protocol"],
                                               Port_service.port == old_port["port"]).execute()

                                    Hostname_Ip.update(changed=datetime.datetime.now())\
                                        .where(Hostname_Ip.ip == old_service["ip"]).execute()

                                    #print(1,"set up")
                                    alert(ip=new_service["ip"],
                                          port=new_port["port"],
                                          protocol=new_port["protocol"],
                                          message=warning_port_up,
                                          service=new_port["name"])

                                if not old_port["name"] == new_port["name"]:
                                    Port_service_history.insert(ip=new_service["ip"],
                                                                port=old_port["port"],
                                                                service=old_port["name"],
                                                                is_up=old_port["open"],
                                                                protocol=old_port["protocol"],
                                                                changed=old_port["changed"]).execute()

                                    Port_service.update(service=new_port["name"],
                                                        changed=datetime.datetime.now)\
                                        .where(Port_service.ip == old_service["ip"],
                                               Port_service.port == old_port["port"],
                                               Port_service.protocol == old_port["protocol"]).execute()

                                    Hostname_Ip.update(changed=datetime.datetime.now())\
                                        .where(Hostname_Ip.ip == old_service["ip"]).execute()

                                    alert(ip=new_service["ip"],
                                          port=new_port["port"],
                                          protocol=new_port["protocol"],
                                          message=warning_change_service.format(old_service["name"]),
                                          service=old_service["name"])

                                    check_port_in_protocols(new_service["ip"], new_port, proto_dict)

                        if not found_port:
                            if old_port["open"]:
                                Port_service_history.insert(ip=new_service["ip"],
                                                            port=old_port["port"],
                                                            service=old_port["name"],
                                                            is_up=old_port["open"],
                                                            protocol=old_port["protocol"],
                                                            changed=old_port["changed"]).execute()

                                Port_service.update(is_up=False,
                                                    changed=datetime.datetime.now())\
                                    .where(Port_service.ip == old_service["ip"],
                                           Port_service.port== old_port["port"],
                                           Port_service.protocol == old_port["protocol"]).execute()

                                Hostname_Ip.update(changed=datetime.datetime.now())\
                                    .where(Hostname_Ip.ip == old_service["ip"]).execute()

                                alert(ip=new_service["ip"],
                                      port=old_port["port"],
                                      protocol=old_port["protocol"],
                                      service=old_port["name"],
                                      message=warning_port_down)


            if not found_ip:
                Port_service_history.insert(ip=old_port["ip"],
                                            port=old_port["port"],
                                            service=old_port["name"],
                                            is_up=old_port["open"],
                                            protocol=old_port["protocol"],
                                            changed=old_port["changed"])

                Port_service.update(is_up=False,
                                    changed=datetime.datetime.now())\
                    .where(Port_service.ip == old_service["ip"]).execute()

                Hostname_Ip.update(changed=datetime.datetime.now())\
                    .where(Hostname_Ip.ip == old_service["ip"]).execute()

                alert(ip = old_service["ip"], message=warning_ip_down)


def new_scan_in_db(new_services):
    update_dicts()
    targets = []
    for target in new_services:
        targets.append(target["ip"])
    old_services = get_old_services_in_dict(Hostname_Ip.select().where(Hostname_Ip.ip << targets)
                                            .join(Port_service, 'left outer', on=Port_service.ip == Hostname_Ip.ip)
                                            .group_by(Hostname_Ip).aggregate_rows())

    check_old_services(old_services, new_services)
    check_new_services(old_services, new_services)

def update_scanner(host, port, key):
    uri = "%s:%i" %( host, port,)
    try:
        Scanners.insert(uri = uri, key = key).execute()
    except Exception:
        Scanners.update(key = key).where(Scanners.uri == uri).execute()
