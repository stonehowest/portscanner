from database import model
import json
from peewee import fn
import netaddr
import hashlib
import re

def getUnreadNotifications(i, network):
    if network:
        ips = list(netaddr.IPNetwork(network))
        alerts = model.Alert_ip.select(model.Alert_ip, model.Alert_port, model.Hostname_Ip,
             fn.COUNT(model.Alert_port.id).alias("number")).where(model.Alert_ip.ip << ips).paginate(i, 30)\
            .join(model.Alert_port, on=model.Alert_port.alert_ip == model.Alert_ip.id)\
            .join(model.Hostname_Ip, on= model.Alert_ip.ip == model.Hostname_Ip.ip).naive().group_by(model.Alert_ip.ip)\
            .having(~model.Alert_ip.is_checked).aggregate_rows()
    else:
        alerts = model.Alert_ip.select(model.Alert_ip,model.Alert_port, model.Hostname_Ip, fn.COUNT(model.Alert_port.id)
            .alias("number") ).paginate(i,30).join(model.Alert_port, on=model.Alert_port.alert_ip == model.Alert_ip.id).\
            join(model.Hostname_Ip, on=model.Alert_ip.ip == model.Hostname_Ip.ip).naive().group_by(model.Alert_ip.ip)\
            .having(~model.Alert_ip.is_checked).aggregate_rows()
    return alerts

def get_filters(i):
    data = model.Rule_filter.select().paginate(i,30)
    return data

def insert_filter(ip, service, port, comment, protocol):
    if port:
        id = model.Rule_filter.insert(ip=ip, port=port, service=service, comment=comment,  protocol=protocol).execute()
    else:
        id = model.Rule_filter.insert(ip=ip, service=service, comment=comment, protocol=protocol).execute()
    if id:
        return json.dumps({'id': id, 'ip': ip, 'service': service, 'port': port, 'comment': comment, 'protocol': protocol})

def edit_filter(id, ip, service, port, comment):
    model.Rule_filter.update(service=service,ip=ip, port=port, comment=comment).where(model.Rule_filter.id == id)
    return json.dumps({'id': id, 'service': service, 'ip': ip, 'port': port, 'comment': comment})

def remove_filter(id):
    model.Rule_filter.delete().where(model.Protocol.id == id).execute()
    return json.dumps({})

def get_protocols(i):
    return model.Protocol.select().order_by(model.Protocol.port).paginate(i, 30)

def insert_protocol(service,protocol, port, comment="", usercomment=0):
    try:
        model.Protocol.select().where(model.Protocol.protocol == protocol, model.Protocol.service == service, model.Protocol.port == port).get()
        return json.dumps({"status": "fail"})
    except model.Protocol.DoesNotExist:
        id = model.Protocol.insert(port=port, protocol=protocol, service=service, comment=comment, user_comment=usercomment).execute()
        return json.dumps({"status": "add", 'id': id, 'protocol': protocol, 'service': service, 'port': port, 'usercomment': usercomment, 'comment':comment })

def edit_protocol(id, service, port, comment):
    model.Protocol.update(service=service, port=port, comment=comment).where(model.Protocol.id == id)
    return json.dumps({'id': id, 'service': service, 'port': port, 'comment':comment})

def remove_protocol(id):
    model.Protocol.delete().where(model.Protocol.id == id).execute()
    return json.dumps({})

def edit_alert_ip(id,ip, service, port, comment):
    model.Alert_ip.update(comment=comment).where(model.Alert_ip.ip == ip)
    return json.dumps({'id': id, 'service': service, 'ip': ip, 'port': port, 'comment': comment})

def edit_alert_port(id, ip, service, port, comment):
    model.Alert_ip.update(comment = comment).where(model.Alert_port.alert_ip == id)
    return json.dumps({'id': id, 'service': service, 'ip': ip, 'port': port, 'comment': comment})

def remove_alert_port(id):
    model.Alert_ip.delete().where(model.Protocol.id == id)
    return True

def insert_in_filter_alert_port(port):
    id = model.Rule_filter.insert(port=port).execute()
    return json.dumps({'id': id, 'service': '', 'ip': '', 'port': port, 'comment': ''})

def update_port(port, ip , comment):
    model.Port_service.update(comment=comment).where(model.Port_service.ip == ip, model.Port_service.port == port)
    return json.dumps({'ip': ip, 'comment': comment})

def insert_in_filter_alert_ip(ip):
    id = model.Rule_filter.insert(ip = ip).execute()
    return json.dumps({'id': id, 'service': '', 'ip':ip, 'port': '','comment': ''})

def remove_alert_ip(id):
    model.Alert_ip.delete().where(model.Protocol.id == id)
    return True

def update_server(ip , comment):
    model.Hostname_Ip.update(comment = comment).where(model.Hostname_Ip.ip == ip)
    return json.dumps({'ip': ip, 'comment': comment})

def get_json_search():
    json_search = []
    for row in model.Hostname_Ip.select(model.Hostname_Ip.hostname, model.Hostname_Ip.ip):
        if row.hostname is not "":
            json_search.append(row.hostname)
        if row.ip is not "":
            json_search.append(row.ip)
    return json.dumps(json_search)

def user_login(id, login):
    if len(login) > 3:
        model.User.update(login=login).where(model.User.id == id).execute()
    return json.dumps({})

def user_first_name(id, first_name):
    model.User.update(first_name=first_name).where(model.User.id == id).execute()
    return json.dumps({})

def user_last_name(id, last_name):
    model.User.update(last_name=last_name).where(model.User.id == id).execute()
    return json.dumps({})

def user_email(id, email):
    model.User.update(email=email).where(model.User.id == id).execute()
    return json.dumps({})

def change_password(login, password):
#check if fields have value
    if not login or not password:
        return json.dumps({"status": "field"})
    else:
        #check if password is strong enough
        if CheckPassword(password):
            changed = model.User.update(password = hashlib.sha1(password.encode()).hexdigest()).where(model.User.login == login).execute()
            if changed:
                return json.dumps({"status":"changed",})
            else:
                return json.dumps({"status":"failed"})
        else:
            return json.dumps({"status": "check-password-failed"})

def CheckPassword(password):
    if re.search('[a-z]',password) and re.search('[A-Z]',password) and len(password) >= 8 and re.search('\d+',password):
        #re.search('.,[,!,@,#,$,%,^,&,*,(,),_,~,-,]',password)
        return True
    else:
        return False