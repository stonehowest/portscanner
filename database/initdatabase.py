import hashlib
import os
import mysql.connector
from database import model
from database.scannerdata import *
import urllib,csv,codecs
import re

def check():
    create_db_if_not_exists()
    if not model.Port_service.table_exists():
        create_db()

def create_db_if_not_exists():
    connection = mysql.connector.connect(host="localhost" , user="root", passwd="root")
    cursor = connection.cursor()
    cursor.execute("create database if not exists nmap_db")
    connection.commit()
    cursor.close()
    connection.close()

def create_db():
    print("initialize...")
    nmap_db.create_tables([Protocol, Hostname_Ip, Hostname_Ip_history, Alert_ip, Alert_port, Port_service_history,Port_service ,Rule_filter , User , Scan_settings, Networks, History_networks, Scanners, Feedback])
    User.insert(active = 1,login = "admin", password = hashlib.sha1('stone@#123'.encode()).hexdigest(), first_name= "admin", last_name= "admin", email="admin@admin.com", type=True).execute(),;
    init_scan_settings()
    init_protocol()
    print("done...")

def init_scan_settings():
    Scan_settings.insert().execute()

def init_protocol():
    try:
        response = urllib.request.urlopen("https://www.iana.org/assignments/service-names-port-numbers/service-names-port-numbers.csv")
        cr = csv.reader(codecs.iterdecode(response , 'utf-8'))
        protocols = []
        for row in cr:
            if row[1] and (row[2] == "udp" or row[2]=="tcp"):
                    if re.match("^[a-zA-Z0-9]*-[-a-zA-Z0-9]*$", row[1]):
                        start = row[1].strip("-")[0]
                        end = row[1].strip("-")[1]
                        for i in range(int(start),int(end)+1):
                            protocols.append({"port": i, "service":row[0], "protocol":row[2], "user_comment":"IANA", "comment":row[3]})
                    else:
                        protocols.append({"port": row[1], "service":row[0], "protocol":row[2], "user_comment":"IANA", "comment":row[3]})
        Protocol.insert_many(protocols).execute()
    except urllib.error.URLError:
        command = 'awk \'{split($2, a,"/"); if (1) print $1, a[1], a[2]}\' /etc/services';
        for line in os.popen(command):
            if not line.startswith("#"):
                row = line.strip().split(" ")
                if row[0]:
                    Protocol.create(port=row[1], service=row[0], protocol=row[2]).save()