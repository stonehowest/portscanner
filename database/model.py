from peewee import *
import datetime
nmap_db = MySQLDatabase("nmap_db",host="localhost", user="root", passwd="root")

from flask.ext.login import UserMixin

class MySQLModel(Model):
    """A base model that will user MYSql db"""
    class Meta:
        database = nmap_db
class User(MySQLModel):
    first_name = CharField()
    last_name = CharField()
    login = CharField(unique=True)
    email = CharField()
    password = CharField()
    active = BooleanField()
    type =  BooleanField(unique=False)

    # Flask-Login integration
    def is_authenticated(self):
        return True
    def is_active(self):
        return True
    def is_anonymous(self):
        return False
    def get_id(self):
        return self.id
    def get_login(self):
        return self.login

    # Required for administrative interface
    def __unicode__(self):
        return self.username


class Protocol(MySQLModel):
    port = IntegerField()
    protocol = CharField()
    service = CharField()
    user_comment = CharField()
    comment = TextField()


class Hostname_Ip(MySQLModel):
    ip = CharField(primary_key=True, unique=True)
    hostname = CharField()
    comment = TextField()
    user_comment = CharField()
    changed = DateTimeField(default= datetime.datetime.now )

class Hostname_Ip_history(MySQLModel):
    ip = CharField()
    hostname = CharField()
    comment = TextField()
    user_comment = CharField()
    changed = DateTimeField(default= datetime.datetime.now )

class Alert_ip(MySQLModel):
    warning = CharField()
    is_checked = BooleanField(default = False)
    comment = CharField()
    user_comment = CharField()
    checked_by = CharField(default="")
    ip = ForeignKeyField(Hostname_Ip, related_name="host")
    changed = DateTimeField(default= datetime.datetime.now)

class Scanners(MySQLModel):
    uri = CharField(primary_key=True)
    key= CharField()

class Alert_port(MySQLModel):
    alert_ip = ForeignKeyField(Alert_ip ,related_name ="alerts", on_delete='CASCADE')
    port = IntegerField()
    service = CharField()
    warning = CharField()
    is_checked = BooleanField(default = False)
    checked_by = CharField(default="")
    comment = CharField()
    protocol = CharField()
    changed = DateTimeField(default= datetime.datetime.now)
    user_comment = CharField()

class Port_service(MySQLModel):
    ip = ForeignKeyField(Hostname_Ip ,related_name ="ports")
    port = IntegerField()
    service = CharField()
    is_up = BooleanField()
    protocol = CharField()
    changed = DateTimeField(default= datetime.datetime.now)
    comment = CharField()
    user_comment = CharField()

class Port_service_history(MySQLModel):
    ip = CharField()
    port = IntegerField()
    service = CharField()
    is_up = BooleanField()
    protocol = CharField()
    changed = DateTimeField(default= datetime.datetime.now)
    comment = CharField()
    user_comment = CharField()

class Scan_settings(MySQLModel):
    threads = IntegerField(default = 200)
    option = CharField(default = "-sT")


class Networks(MySQLModel):
    network = CharField()
    scan = ForeignKeyField(Scan_settings,related_name="networks")

class Rule_filter(MySQLModel):
    ip = CharField()
    port = IntegerField()
    comment = CharField()
    protocol = CharField()
    service = CharField()
    user_comment = CharField()

class Feedback(MySQLModel):
    comment = CharField()
    date = DateTimeField(default= datetime.datetime.now)
    is_fixed = BooleanField(default=False)
    user = CharField()


class History_networks(MySQLModel):
    network = CharField()



def __str__(self):
    r = {}
    for k in self._data.keys():
      try:
         r[k] = str(getattr(self, k))
      except:
         r[k] = json.dumps(getattr(self, k))
    return str(r)


def is_closed():
    nmap_db.is_closed()

def connect():
    nmap_db.connect()
    return nmap_db

def close():
    nmap_db.close()